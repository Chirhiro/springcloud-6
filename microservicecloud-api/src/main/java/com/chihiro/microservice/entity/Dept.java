package com.chihiro.microservice.entity;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;


// @AllArgsConstructor全参数的构造方法
@NoArgsConstructor  //无参的构造方法
@Data    //自动给参数生成get和set方法
@Accessors(chain = true)  //开启连式
public class Dept implements Serializable {

    private int deptno;//主键

    private String dname;//部门名称

    private String db_source;//来自哪个数据库

    public Dept(String dname) {
        this.dname = dname;
    }
}
