package com.chihiro.microservice.service;

import com.chihiro.microservice.entity.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DeptClientServiceFallbackFactory implements FallbackFactory<DeptClientService> {
    @Override
    public DeptClientService create(Throwable throwable) {
        return new DeptClientService() {
            @Override
            public Dept list(int deptno) {
                Dept dept=new Dept();
                dept.setDeptno(deptno).setDname("该ID："+deptno +"没有对应的信息，Consumer客户端提供的降级信息，此刻服务已关闭").setDb_source("no this database in Mysql");
                return dept;
            }

            @Override
            public List<Dept> list() {
                return null;
            }
        };
    }
}
