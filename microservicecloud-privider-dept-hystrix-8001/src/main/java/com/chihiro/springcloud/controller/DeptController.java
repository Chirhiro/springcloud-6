package com.chihiro.springcloud.controller;

import com.chihiro.microservice.entity.Dept;
import com.chihiro.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DeptController {

    @Autowired
    private DeptService deptService;

    @Autowired
    private DiscoveryClient client;

    /**
     * 部门查询
     * @param deptno
     * @return
     */
    @RequestMapping(value = "/dept/list/{deptno}",method = RequestMethod.GET)
    @ResponseBody
    //一旦调用服务方法失败并抛出异常错误信息后，会自动调用@HystrixCommand标注好的fallbackMethod指定的方法
    @HystrixCommand(fallbackMethod = "processHystrix_Get")
    public Dept list(@PathVariable("deptno") int deptno){
        Dept dept = deptService.find(deptno);
        if (dept == null) {
            throw new RuntimeException("该ID："+deptno +"没有对应的信息");
        }
        return dept;
    }



    public Dept processHystrix_Get(@PathVariable("deptno") int deptno){
        Dept dept=new Dept();
        dept.setDeptno(deptno).setDname("该ID："+deptno +"没有对应的信息，null").setDb_source("no this database in Mysql");
        return dept;
    }

}
