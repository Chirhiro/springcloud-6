package com.chihiro.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient  /*本服务启动后会自动注册进Eureka服务列表内*/
@EnableDiscoveryClient /*开启服务发现 --*/
@EnableCircuitBreaker //对Hystrix熔断机制的支持
public class DeptPrividerHystrix8001_App {

    public static void main(String[] args) {
        SpringApplication.run(DeptPrividerHystrix8001_App.class,args);
    }
}
