package com.chihiro.microservice.cfgbeans;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration   // -->spring  的 applicationContext.xml
public class ConfigBean {


    @Bean
    @LoadBalanced //负载均衡工具
    public RestTemplate getUserSeriver(){

        return new RestTemplate();
    }

    //使用自己重新声明的随机算法替代默认的轮询算法
   /* @Bean
    public IRule myIRule(){
        return  new RandomRule();
    }
*/
}
