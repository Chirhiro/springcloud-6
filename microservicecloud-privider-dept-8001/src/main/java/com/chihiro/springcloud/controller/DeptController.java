package com.chihiro.springcloud.controller;

import com.chihiro.microservice.entity.Dept;
import com.chihiro.springcloud.service.DeptService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DeptController {

    @Autowired
    private DeptService deptService;

    @Autowired
    private DiscoveryClient client;

    /**
     * 部门查询
     * @param deptno
     * @return
     */
    @RequestMapping(value = "/dept/list/{deptno}",method = RequestMethod.GET)
    @ResponseBody
    public Dept list(@PathVariable("deptno") int deptno){
        Dept dept = deptService.find(deptno);
        if (dept == null) {
            throw new RuntimeException("该ID："+deptno +"没有对应的信息");
        }
        return dept;
    }

    /**
     * 查询所有部门
     * @return
     */
    @RequestMapping(value = "/dept/list", method = RequestMethod.GET)
    @ResponseBody
    public List<Dept> list()
    {
        return deptService.list();
    }


    /**
     * @Autowired
     * private DiscoveryClient client;
     *服务发现
     * @return
     */
    @RequestMapping(value = "/dept/discovery", method = RequestMethod.GET)
    @ResponseBody
    public Object discovery()
    {
        //拿取注册在Eureka中心的所有服务
        List<String> list=client.getServices();
        System.out.println("********"+list);

        //拿取一个名称MICROSERVICECLOUD-DEPT
        List<ServiceInstance> disList=client.getInstances("MICROSERVICECLOUD-DEPT");
        for (ServiceInstance serviceInstance : disList) {
            System.out.println(serviceInstance.getServiceId()+"\t"+serviceInstance.getHost()+"\t"+serviceInstance.getUri());
        }
        return this.client;
    }

}
